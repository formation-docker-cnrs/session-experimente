terraform {
  required_providers {
    docker = {
      source  = "kreuzwerker/docker"
      version = "3.0.2"
    }
  }
}

variable "docker_api_path" {
  type        = string
  description = "Chemin d'accès à l'API (via tcp ou unix)"
}

provider "docker" {
  # Pour un hôte linux, déclaration de l'hôte : 
  # "unix:///var/run/docker.sock"
  # Adaptation pour macOS :
  # "unix:///$HOME/.docker/run/docker.sock"
  host = var.docker_api_path
}

resource "docker_container" "terraform_container" {
  image = "local/ubu-form-cnrs-nginx:22.04.2"
  name  = "nginx_via_terraform"
  ports {
    internal = "80"
    external = "5555"
  }
}