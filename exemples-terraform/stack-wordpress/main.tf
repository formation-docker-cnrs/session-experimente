terraform {
  required_providers {
    docker = {
      source  = "kreuzwerker/docker"
      version = "3.0.2"
    }
  }
}

variable "docker_api_path" {
  type        = string
  description = "Chemin d'accès à l'API (via tcp ou unix)"
}

provider "docker" {
  host = var.docker_api_path
}

resource "docker_network" "private_network" {
  name = "wpnet_form_docker"
}

resource "docker_volume" "wpdb_form_docker" {
  name = "wpdb_form_docker"
}

resource "docker_volume" "wphtml_form_docker" {
  name = "wphtml_form_docker"
}

resource "docker_container" "db_form_docker" {
  name  = "db"
  image = "mariadb:latest"
  restart = "always"
  network_mode = "wpnet_form_docker"
  mounts {
    type = "volume"
    target = "/var/lib/mysql"
    source = "wpdb_form_docker"
  }
  env = [
     "MYSQL_ROOT_PASSWORD=mdprootmysql",
     "MYSQL_DATABASE=wordpress",
     "MYSQL_USER=wpuser",
     "MYSQL_PASSWORD=wppassword"
  ]
}

resource "docker_container" "wordpress_form_docker" {
  name  = "wordpress"
  image = "wordpress:latest"
  restart = "always"
  network_mode = "wpnet_form_docker"
  env = [
    "WORDPRESS_DB_HOST=db",
    "WORDPRESS_DB_USER=wpuser",
    "WORDPRESS_DB_PASSWORD=wppassword",
    "WORDPRESS_DB_NAME=wordpress"
  ]
  ports {
    internal = "80"
    external = "5555"
  }
  mounts {
    type = "volume"
    target = "/var/www/html"
    source = "wphtml_form_docker"
  }
}