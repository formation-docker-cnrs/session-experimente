# Pull d'une image Ubuntu 22.04 LTS
source "docker" "ubu-form-cnrs-nginx" {
    image = "ubuntu:jammy"
    commit = true
    run_command = [
      "-d", "-i", "-t", "{{.Image}}", "/bin/bash"
    ]
    changes = [
      "WORKDIR /var/www/html",
      "ENV HOSTNAME localhost",
      "EXPOSE 80 443",
      "LABEL version=1.0.0",
      "ONBUILD RUN date",
      "ENTRYPOINT [\"nginx\", \"-g\", \"daemon off;\"]",
    ]
}

# Construire l'image "personnalisée"
build {
  sources = [
    "source.docker.ubu-form-cnrs-nginx"
  ]

  provisioner "shell" {
    inline = [
      "apt-get update",
      "DEBIAN_FRONTEND=noninteractive apt-get -qq -y install curl nginx",
      "apt-get clean autoclean",
      "apt-get autoremove --yes",
      "rm -fr /var/lib/apt/lists/*",
      # commande à éviter  ci-après : vous  ne  
      # gagnez que  quelques Ko  et plus  rien 
      # ne peut dériver de cette image de base 
      # rm -rf /var/lib/{apt,dpkg,cache,log}/
      "ln -sf /dev/stdout /var/log/nginx/access.log",
      "ln -sf /dev/stderr /var/log/nginx/error.log",
    ]
  }

  # Tag de l'image Docker créé + personnalisation
  post-processors {
      post-processor "docker-tag" {
        repository = "local/ubu-form-cnrs-nginx"
        tags = [
          "22.04.2",
          "latest"
        ]
    }
  }
}