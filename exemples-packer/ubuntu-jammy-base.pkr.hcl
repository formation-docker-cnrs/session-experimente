# Pull d'une image Ubuntu 22.04 LTS
source "docker" "ubuntu-formation-cnrs" {
    image = "ubuntu:jammy"
    commit = true
    run_command = [
      "-d", "-i", "-t", "{{.Image}}", "/bin/bash"
    ]
}

# Construire l'image "personnalisée"
build {
  sources = [
    "source.docker.ubuntu-formation-cnrs"
  ]

  # Tag de l'image Docker créé
  post-processors {
    post-processor "docker-tag" {
      repository =  "local/ubuntu-formation-cnrs"
      tags = [
        "22.04.2",
        "latest"
      ]
    }
  }
}